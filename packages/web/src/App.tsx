import { defineComponent } from 'vue';
import TestComponent from './components/TestComponent';

export default defineComponent({
  setup() {
    return () => {
      return <TestComponent />;
    };
  },
});
