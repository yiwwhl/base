import { defineComponent } from 'vue';
import * as infra from '@infra/index';

export default defineComponent({
  setup() {
    return () => {
      return (
        <>
          <infra.TestCompo />
          <h1>Test Component</h1>
        </>
      );
    };
  },
});
